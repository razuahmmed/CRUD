<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Add Product</title>

        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/css/materialize.min.css"/>
        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/css/icon.css"/>
        <!-- custom CSS -->
        <style type="text/css">
            .width-30-pct{
                width:30%;
            }

            .text-align-center{
                text-align:center;
            }

            .margin-bottom-1em{
                margin-bottom:1em;
            }
        </style>
    </head>
    <body>
        <div class="container" ng-app="myApp" ng-controller="productsCtrl">
            <div class="row">
                <div class="col s12">
                    <h4>Product</h4>

                    <!-- modal for for creating new product -->
                    <div id="modal-product-form" class="">
                        <div class="modal-content">
                            <h4 id="modal-product-title">Create New Product</h4>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input ng-model="name" type="text" class="validate" id="form-name" placeholder="Type name here..." />
                                    <label for="name">Name</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea ng-model="description" type="text" class="validate materialize-textarea" placeholder="Type description here..."></textarea>
                                    <label for="description">Description</label>
                                </div>
                                <div class="input-field col s12">
                                    <input ng-model="price" type="text" class="validate" id="form-price" placeholder="Type price here..." />
                                    <label for="price">Price</label>
                                </div>
                                <div class="input-field col s12">
                                    <a id="btn-create-product" class="waves-effect waves-light btn margin-bottom-1em" ng-click="createProduct()"><i class="material-icons left">add</i>Create</a>
                                    <a id="btn-update-product" class="waves-effect waves-light btn margin-bottom-1em" ng-click="updateProduct()"><i class="material-icons left">edit</i>Save Changes</a>
                                    <a class="modal-action modal-close waves-effect waves-light btn margin-bottom-1em"><i class="material-icons left">close</i>Close</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- floating button for creating product -->
                    <div class="fixed-action-btn" style="bottom:45px; right:24px;">
                        <a class="waves-effect waves-light btn modal-trigger btn-floating btn-large red" href="#modal-product-form" ng-click="showCreateForm()"><i class="large material-icons">add</i></a>
                    </div>
                </div> 
            </div> 
        </div>

        <script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/js/materialize.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/js/angular.min.js"></script>
        <script type="text/javascript">
            var app = angular.module('myApp', []);
            app.controller('productsCtrl', function ($scope, $http) {
                $scope.showCreateForm = function () {

                    $scope.clearForm();
                    $('#modal-product-title').text("Create New Product");
                    $('#btn-update-product').hide();
                    $('#btn-create-product').show();
                };

                $scope.createProduct = function () {
                    $http.post('addProduct', {
                        'name': $scope.name,
                        'description': $scope.description,
                        'price': $scope.price
                    }).success(function (data, status, headers, config) {

                        Materialize.toast(data["maps"]["messageStr"], 4000);
                        $scope.clearForm();
                        $('#modal-product-form').closeModal();
                    });
                };

                $scope.clearForm = function () {
                    $scope.id = "";
                    $scope.name = "";
                    $scope.description = "";
                    $scope.price = "";
                };
            });

            $(document).ready(function () {
                // initialize modal
                $('.modal-trigger').leanModal();
            });
        </script>
    </body>
</html>