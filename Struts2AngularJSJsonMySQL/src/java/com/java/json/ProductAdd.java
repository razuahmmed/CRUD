package com.java.json;

import com.java.db.ProductDB;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.ServletActionContext;

public class ProductAdd {

    static HttpServletRequest request;
    private Map<String, String> maps = new HashMap<String, String>();

    /**
     * @return the maps
     */
    public Map<String, String> getMaps() {
        return maps;
    }

    /**
     * @param maps the maps to set
     */
    public void setMaps(Map<String, String> maps) {
        this.maps = maps;
    }

    public String execute() {
        try {
            request = ServletActionContext.getRequest();
            String filename = IOUtils.toString(request.getInputStream());

            int status = ProductDB.fileToString(filename);
            if (status > 0) {
                maps.put("messageStr", "Product has been successfully added");
            } else {
                maps.put("messageStr", "Product add failed");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return SUCCESS;
    }
}
