package com.java.json;

import com.java.db.ProductDB;
import com.java.persistance.Product;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.ArrayList;
import java.util.List;

public class ProductReadJSONAction {

    private List<Product> allProduct = null;

    public ProductReadJSONAction() {

        Product pd = new Product();
        allProduct = new ArrayList<Product>();

        List<Product> pList = ProductDB.selectAll();
        for (Product p : pList) {
            pd.setName(p.getName());
            pd.setId(p.getId());
            pd.setDescription(p.getDescription());
            pd.setPrice(p.getPrice());
            allProduct.add(p);
        }
    }

    /**
     * @return the allProduct
     */
    public List<Product> getAllProduct() {
        return allProduct;
    }

    /**
     * @param allProduct the allProduct to set
     */
    public void setAllProduct(List<Product> allProduct) {
        this.allProduct = allProduct;
    }

    public String execute() {
        return SUCCESS;
    }
}
