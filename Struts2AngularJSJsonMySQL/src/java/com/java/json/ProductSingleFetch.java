package com.java.json;

import com.java.db.ProductDB;
import com.java.persistance.Product;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.ServletActionContext;

public class ProductSingleFetch {

    static HttpServletRequest request;
    private Product singleData = null;

    /**
     * @return the singleData
     */
    public Product getSingleData() {
        return singleData;
    }

    /**
     * @param singleData the singleData to set
     */
    public void setSingleData(Product singleData) {
        this.singleData = singleData;
    }

    public String execute() throws Exception {

        singleData = new Product();
        request = ServletActionContext.getRequest();

        String filename = IOUtils.toString(request.getInputStream());

        List<Product> pp = ProductDB.fileToStringOne(filename);
        for (Product p : pp) {
            singleData.setId(p.getId());
            singleData.setName(p.getName());
            singleData.setDescription(p.getDescription());
            singleData.setPrice(p.getPrice());
        }

        return SUCCESS;
    }
}
