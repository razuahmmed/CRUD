package com.java.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionConfiguration {

    public static final String URL = "jdbc:mysql://localhost:3306/strutsangularcrud";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "123456";

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) {
        System.out.println("Conn " + getConnection());
    }
}
