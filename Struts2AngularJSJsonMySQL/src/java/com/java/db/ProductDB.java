package com.java.db;

import com.java.persistance.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ProductDB {

    public static int fileToString(String filename) {

        int status = 0;
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(filename);
            JSONObject json = (JSONObject) obj;

            String name = (String) json.get("name");
            String description = (String) json.get("description");
            int price = Integer.parseInt((String) json.get("price"));

            status = ProductDB.insert(name, description, price);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return status;
    }

    public static List<Product> fileToStringOne(String filename) {

        List<Product> p = new ArrayList<>();
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(filename);
            JSONObject json = (JSONObject) obj;

            int id = Integer.parseInt(String.valueOf(json.get("id")));

            p = ProductDB.selectOne(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return p;
    }

    public static int insert(String name, String description, int price) {

        int status = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO products (name,description,price) VALUES (?, ?,?)");

            preparedStatement.setString(1, name);
            preparedStatement.setString(2, description);
            preparedStatement.setInt(3, price);

            status = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return status;
    }

    public static List<Product> selectAll() {

        List<Product> persons = new ArrayList<Product>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM products");

            while (resultSet.next()) {
                Product person = new Product();
                person.setId(resultSet.getInt("id"));
                person.setName(resultSet.getString("name"));
                person.setPrice(resultSet.getInt("price"));
                person.setDescription(resultSet.getString("description"));

                persons.add(person);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return persons;
    }

    public static List<Product> selectOne(int id) {

        List<Product> persons = new ArrayList<Product>();

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM products WHERE id='" + id + "' ");

            while (resultSet.next()) {
                Product person = new Product();
                person.setId(resultSet.getInt("id"));
                person.setName(resultSet.getString("name"));
                person.setPrice(resultSet.getInt("price"));
                person.setDescription(resultSet.getString("description"));

                persons.add(person);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return persons;
    }

    public static int jsonDataForUpdate(String filename) {

        int status = 0;
        Product p = new Product();
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(filename);
            JSONObject json = (JSONObject) obj;

            int id = Integer.parseInt(String.valueOf(json.get("id")));
            String name = (String) json.get("name");
            String description = (String) json.get("description");
            int price = Integer.parseInt(String.valueOf(json.get("price")));

            p.setId(id);
            p.setName(name);
            p.setDescription(description);
            p.setPrice(price);

            status = update(p, id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return status;
    }

    public static int update(Product person, int id) {

        int status = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE products SET name = ?, description = ?, price = ?  WHERE id = ?");

            preparedStatement.setString(1, person.getName());
            preparedStatement.setString(2, person.getDescription());
            preparedStatement.setInt(3, person.getPrice());
            preparedStatement.setInt(4, id);

            status = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return status;
    }

    public static int jsonDataForDelete(String filename) {

        int status = 0;
        Product p = new Product();
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(filename);
            JSONObject json = (JSONObject) obj;

            int id = Integer.parseInt(String.valueOf(json.get("id")));

            status = delete(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return status;
    }

    public static int delete(int id) {

        int status = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM products WHERE id = ?");

            preparedStatement.setInt(1, id);

            status = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return status;
    }
}
