<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Display Products</title>

        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/css/materialize.min.css"/>
        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/css/icon.css"/>
        <!-- custom CSS -->
        <style type="text/css">
            .width-30-pct{
                width:30%;
            }

            .text-align-center{
                text-align:center;
            }

            .margin-bottom-1em{
                margin-bottom:1em;
            }
        </style>
    </head>
    <body>
        <div class="container" ng-app="myApp" ng-controller="productsCtrl">
            <div class="row">
                <div class="col s12">
                    <h4>Products</h4>

                    <div id="modal-product-form" class="">
                        <div class="modal-content">
                            <h4 id="modal-product-title">Create New Product</h4>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input ng-model="name" type="text" class="validate" id="form-name" placeholder="Type name here..." />
                                    <label for="name">Name</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea ng-model="description" type="text" class="validate materialize-textarea" placeholder="Type description here..."></textarea>
                                    <label for="description">Description</label>
                                </div>
                                <div class="input-field col s12">
                                    <input ng-model="price" type="text" class="validate" id="form-price" placeholder="Type price here..." />
                                    <label for="price">Price</label>
                                </div>
                                <div class="input-field col s12">
                                    <a id="btn-create-product" class="waves-effect waves-light btn margin-bottom-1em" ng-click="createProduct()"><i class="material-icons left">add</i>Create</a>
                                    <a id="btn-update-product" class="waves-effect waves-light btn margin-bottom-1em" ng-click="updateProduct()"><i class="material-icons left">edit</i>Save Changes</a>
                                    <a class="modal-action modal-close waves-effect waves-light btn margin-bottom-1em"><i class="material-icons left">close</i>Close</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- used for searching the current list -->
                    <input type="text" ng-model="search" class="form-control" placeholder="Search product..." />
                    <!-- table that shows product record list -->
                    <table class="hoverable bordered">
                        <thead>
                            <tr>
                                <th class="text-align-center">ID</th>
                                <th class="width-30-pct">Name</th>
                                <th class="width-30-pct">Description</th>
                                <th class="text-align-center">Price</th>
                                <th class="text-align-center">Action</th>
                            </tr>
                        </thead>
                        <tbody ng-init="getAll()">
                            <tr ng-repeat="d in names| filter:search">
                                <td class="text-align-center">{{ d.id}}</td>
                                <td>{{ d.name}}</td>
                                <td>{{ d.description}}</td>
                                <td class="text-align-center">{{ d.price}}</td>
                                <td>
                                    <a ng-click="getOne(d.id)" class="waves-effect waves-light btn margin-bottom-1em"><i class="material-icons left"></i>Edit</a>
                                    <a ng-click="deleteProduct(d.id)" class="waves-effect waves-light btn margin-bottom-1em"><i class="material-icons left"></i>Delete</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
            </div> 
        </div>

        <script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/js/materialize.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/js/angular.min.js"></script>
        <script type="text/javascript">
                                        var app = angular.module('myApp', []);
                                        app.controller('productsCtrl', function ($scope, $http) {

                                            $scope.getAll = function () {
                                                $http.get("readProducts").success(function (response) {
                                                    $scope.names = response.allProduct;
                                                });
                                            };

                                            $scope.getOne = function (id) {

                                                $('#modal-product-title').text("Edit Product");
                                                $('#btn-update-product').show();
                                                $('#btn-create-product').hide();

                                                $http.post("readOneProduct", {
                                                    'id': id
                                                }).success(function (data, status, headers, config) {

                                                    $scope.id = data['singleData']["id"];
                                                    $scope.name = data['singleData']["name"];
                                                    $scope.description = data['singleData']["description"];
                                                    $scope.price = data['singleData']["price"];

                                                    $('#modal-product-form').openModal();
                                                }).error(function (data, status, headers, config) {
                                                    Materialize.toast('Unable to retrieve records.', 4000);
                                                });
                                            };

                                            $scope.createProduct = function () {
                                                $http.post('addProduct', {
                                                    'name': $scope.name,
                                                    'description': $scope.description,
                                                    'price': $scope.price
                                                }).success(function (data, status, headers, config) {

                                                    Materialize.toast(data["maps"]["messageStr"], 4000);
                                                    $scope.getAll();
                                                    $scope.clearForm();
                                                    $('#modal-product-form').closeModal();
                                                });
                                            };

                                            $scope.updateProduct = function () {
                                                $http.post('updateProduct', {
                                                    'id': $scope.id,
                                                    'name': $scope.name,
                                                    'description': $scope.description,
                                                    'price': $scope.price
                                                }).success(function (data, status, headers, config) {

                                                    Materialize.toast(data["maps"]["messageStr"], 4000);
                                                    $scope.getAll();
                                                    $scope.clearForm();
                                                    $('#modal-product-form').closeModal();
                                                });
                                            };

                                            $scope.deleteProduct = function (id) {
                                                if (confirm("Are you sure?")) {
                                                    $http.post("deleteProduct", {
                                                        'id': id
                                                    }).success(function (data, status, headers, config) {

                                                        Materialize.toast(data["maps"]["messageStr"], 4000);
                                                        $scope.getAll();
                                                    });
                                                }
                                            };

                                            $scope.clearForm = function () {
                                                $scope.id = "";
                                                $scope.name = "";
                                                $scope.description = "";
                                                $scope.price = "";
                                            };
                                        });

                                        $(document).ready(function () {
                                            // initialize modal
                                            $('.modal-trigger').leanModal();
                                        });
        </script>
    </body>
</html>